#
# Simple Makefile to bring in vulkan and glib
#

CC = gcc

GLIB_CFLAGS = $(shell pkg-config --cflags glib-2.0)

CFLAGS = -ggdb -O0 -std=c99 -Wall -Wextra $(GLIB_CFLAGS)

VULKAN_LIBS = $(shell pkg-config --libs vulkan)
GLIB_LIBS = $(shell pkg-config --libs glib-2.0)

LDFLAGS = $(VULKAN_LIBS) $(GLIB_LIBS)

TARGETS = devices memory

.PHONY: all clean

all: $(TARGETS)

%: %.c
	$(CC) $(CFLAGS) $^ -o $@ $(SRC) $(LDFLAGS)

clean:
	$(RM) $(TARGETS)
