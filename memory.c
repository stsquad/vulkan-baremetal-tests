
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <vulkan/vulkan.h>
#include "enum_to_str.h"

#define MAP_SIZE 1024 // Size of the memory mapping

const char *extensions[] = { "VK_EXT_acquire_drm_display" };

#define ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

VkResult map_and_test_memory(VkDevice dev,
                             VkPhysicalDeviceMemoryProperties mem_props,
                             uint32_t index)
{
    VkMemoryAllocateInfo allocInfo = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = MAP_SIZE,
        .memoryTypeIndex = index
    };

    VkDeviceMemory deviceMemory;

    VkResult result = vkAllocateMemory(dev, &allocInfo, NULL, &deviceMemory);

    if (result != VK_SUCCESS) {
        fprintf(stderr, "Failed to allocate memory: %d\n", result);
        return result;
    }

    void* data;
    result = vkMapMemory(dev, deviceMemory, 0, MAP_SIZE, 0, &data);
    if (result != VK_SUCCESS) {
        fprintf(stderr, "Failed to map memory: %d\n", result);
        vkFreeMemory(dev, deviceMemory, NULL);
        return result;
    }

    /*
     * fill in with read and write to region
     */
    printf("  mapped memory to %p\n", data);

    for (int i = 0; i < MAP_SIZE; i += sizeof(uint64_t)) {
        uint64_t *ptr = data + i;
        uint64_t val = *ptr;
        if (val != 0) {
            printf("  unzerroed memory @ %p\n", ptr);
            printf("  skipping 0 check for rest of map (size=%d)\n", MAP_SIZE);
            break;
        }
    }

    printf("  read mapped memory\n");

    for (int i = 0; i < MAP_SIZE; i += sizeof(uint64_t)) {
        uint64_t *ptr = data + i;
        *ptr = i;
    }

    printf("  wrote to mapped memory\n");

    for (int i = 0; i < MAP_SIZE; i += sizeof(uint64_t)) {
        uint64_t *ptr = data + i;
        if (*ptr != i) {
            fprintf(stderr, "%s: didn't get expect value %"PRIx32" != %"PRIx32" @ %p\n",
                    __func__, *ptr, i, ptr);
        }
    }

    printf("  finished checking memory\n");

    vkUnmapMemory(dev, deviceMemory);
    vkFreeMemory(dev, deviceMemory, NULL);

    return VK_SUCCESS;
}

int main(int argc, char *argv[])
{

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <device_index>\n", argv[0]);
        return EXIT_FAILURE;
    }

    int device_index = atoi(argv[1]);

    VkInstanceCreateInfo instance_info = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        NULL, /* pNext extension */
        0,    /* VkInstanceCreateFlags */
        NULL, /* Application Info */
        0, NULL, /* no Enabled Layers */
        ARRAY_LEN(extensions), extensions, /* Extensions */
    };

    VkInstance inst;
    uint32_t count;
    VkPhysicalDevice *devices;
    VkResult res = vkCreateInstance(&instance_info, NULL, &inst);

    g_assert( res == VK_SUCCESS);

    /* First count how many */
    vkEnumeratePhysicalDevices(inst, &count, NULL);

    if (device_index >= count || device_index < 0) {
        fprintf(stderr, "Invalid device index. Please choose between 0 and %d.\n", count - 1);
        return EXIT_FAILURE;
    }

    devices = g_new0(VkPhysicalDevice, count);
    vkEnumeratePhysicalDevices(inst, &count, devices);

    VkPhysicalDevice selected_device = devices[device_index];
    vkGetPhysicalDeviceQueueFamilyProperties(selected_device, &count, NULL);
    VkQueueFamilyProperties *qProps = g_new0(VkQueueFamilyProperties, count);

    for (int i = 0; i < count; i++) {
        printf("  queue %d/%d: flags 0x%x\n",
               i,
               qProps[i].queueCount,
               qProps[i].queueFlags);
    }

    float pri = 1.0;

    VkDeviceQueueCreateInfo queue_info = {
        VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        NULL, /* pNext extension */
        0,    /* VkDeviceQueueCreateFlags */
        0, count, &pri,
    };

    VkDeviceCreateInfo device_info = {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        NULL, /* pNext */
        0, /* flags (reserved) */
        1, &queue_info,
        0, NULL, /* enableLayer (deprecated) */
        0, NULL,
        NULL /* pEnabledFeatures */
    };
    VkDevice dev;

    res = vkCreateDevice(selected_device, &device_info, NULL, &dev);

    g_assert( res == VK_SUCCESS);

    VkPhysicalDeviceMemoryProperties mem_props;
    vkGetPhysicalDeviceMemoryProperties(selected_device, &mem_props);

    int fail_count = 0;

    for (uint32_t i = 0; i < mem_props.memoryTypeCount; i++) {
        VkMemoryType type = mem_props.memoryTypes[i];

        printf("  Memory %d from heap %d, flags 0x%x\n",
               i, type.heapIndex, type.propertyFlags);

        /* check if it is host accessible first */
        if (!(type.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)) {
            printf( "  skipping non HOST_VISIBLE region\n");
        } else {
            VkResult result = map_and_test_memory(dev, mem_props, i);
            if (result != VK_SUCCESS) {
                fprintf(stderr, "Error mapping and testing memory type %d\n", i);
                fail_count++;
            }
        }
    }

    return fail_count ? EXIT_FAILURE : EXIT_SUCCESS;
}
