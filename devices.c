/*
 * Enumerate the Vulkan Devices on a system
 *
 * Copyright (c) 2024 Linaro Ltd
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <vulkan/vulkan.h>

#include "enum_to_str.h"

const char *extensions[] = { "VK_KHR_display", "VK_EXT_acquire_drm_display", "VK_KHR_get_physical_device_properties2" };

#define ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

static void dump_devices(VkPhysicalDevice dev)
{
    VkPhysicalDeviceDriverProperties driverProperties = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES,
        .pNext = NULL
    };
    VkPhysicalDeviceProperties2 deviceProperties2 = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
        .pNext = &driverProperties
    };
    VkPhysicalDeviceProperties *deviceProperties;
    VkPhysicalDeviceDriverProperties *deviceDriverProperties;
    VkPhysicalDeviceMemoryProperties mem_prop;
    g_autoptr(GString) buf = g_string_new("Device Info:\n");

    vkGetPhysicalDeviceProperties2(dev, &deviceProperties2);

    deviceProperties = &deviceProperties2.properties;

    g_string_append_printf(buf, "  Device Name: %s\n", deviceProperties->deviceName);
    g_string_append_printf(buf, "  Device Type: %s\n",
                           device_to_str(deviceProperties->deviceType));
    if (VK_API_VERSION_VARIANT(deviceProperties->apiVersion)) {
        g_string_append_printf(buf, "  !!API Variant!!: %d\n",
                               VK_API_VERSION_VARIANT(deviceProperties->apiVersion));
    }
    g_string_append_printf(buf, "  API Version: %d.%d.%d\n",
                           VK_API_VERSION_MAJOR(deviceProperties->apiVersion),
                           VK_API_VERSION_MINOR(deviceProperties->apiVersion),
                           VK_API_VERSION_PATCH(deviceProperties->apiVersion));

    /*
     * See what we got for the extended info - it should match what
     * we asked for.
     */
    deviceDriverProperties = &driverProperties;

    g_string_append_printf(buf, "  driverName: %s\n", deviceDriverProperties->driverName);
    g_string_append_printf(buf, "  driverInfo: %s\n", deviceDriverProperties->driverInfo);
    g_string_append_printf(buf, "  driverId: %d\n", deviceDriverProperties->driverID);
    
    vkGetPhysicalDeviceMemoryProperties(dev, &mem_prop);

    g_string_append(buf, "  Memory:\n");
    for (uint32_t i = 0; i < mem_prop.memoryHeapCount; ++i) {
       g_string_append_printf(buf, "    Heap %d: size %lu (%s)\n", i,
                              mem_prop.memoryHeaps[i].size,
                              heap_to_str(mem_prop.memoryHeaps[i].flags));
   }

    for (uint32_t i = 0; i < mem_prop.memoryTypeCount; i++)
    {
        VkMemoryType type = mem_prop.memoryTypes[i];
        g_string_append_printf(buf,
                               "    Memory %d is from heap %d with flags 0x%x\n",
                               i, type.heapIndex, type.propertyFlags);
    }

    printf("%s", buf->str);
}

int main (int c, char *argv[])
{
    VkInstanceCreateInfo instance_info = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        NULL, /* pNext extension */
        0,    /* VkInstanceCreateFlags */
        NULL, /* Application Info */
        0, NULL, /* no Enabled Layers */
        ARRAY_LEN(extensions), extensions, /* Extensions */
    };

    VkInstance inst;
    VkResult res;

    res = vkCreateInstance(&instance_info, NULL, &inst);

    if ( res == VK_SUCCESS ) {
        uint32_t count;
        VkPhysicalDevice *devices;

        /* First count how many */
        vkEnumeratePhysicalDevices(inst, &count, NULL);
        printf("I have counted %"PRIx32" vulkan devices\n", count);

        devices = g_new0(VkPhysicalDevice, count);
        vkEnumeratePhysicalDevices(inst, &count, devices);

        for (uint32_t i = 0; i  < count; i++) {
            dump_devices(devices[i]);
        }
    } else {
        fprintf(stderr, "failed to create VK instance (%d)\n", (int) res);
        return -1;
    }
}
