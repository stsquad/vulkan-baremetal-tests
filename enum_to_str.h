/*
 * Mapping enumerated types to strings
 *
 * Copyright (c) 2024 Linaro Ltd
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <vulkan/vulkan.h>

static inline const char* device_to_str(VkPhysicalDeviceType type) {
    switch (type) {
        case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "Other";
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "Integrated GPU";
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "Discrete GPU";
        case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "Virtual GPU";
        case VK_PHYSICAL_DEVICE_TYPE_CPU: return "CPU";
        default: return "Unknown";
    }
}

static inline const char* heap_to_str(VkMemoryHeapFlags flags) {
    if (flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) return "Device local";
    if (flags & VK_MEMORY_HEAP_MULTI_INSTANCE_BIT) return "Multi-instance";
    return "Unknown";
}
